package com.ena.luademo.enums;

/**
 * 限流类型
 *
 * @Author zgr
 * @create 2023/8/8 12:08
 */
public enum LimitTypeEnum {

    /**
     * 自定义
     */
    CUSTOMER,
    /**
     * 请求者ip
     */
    IP;
}
