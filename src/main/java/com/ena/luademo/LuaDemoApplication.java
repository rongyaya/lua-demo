package com.ena.luademo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LuaDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(LuaDemoApplication.class, args);
    }

}
